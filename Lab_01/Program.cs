﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_01
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook nb = new Notebook();
            nb.people.Add(new Person(1, "Anna", "Shmidt", 12345, "USA", "01.12.3456"));
            nb.people.Add(new Person(2, "Ivan", "Kolesnikov", 12345, "Russia", "01.12.3456"));
            nb.people.Add(new Person(3, "Petr", "Galichev", 12345, "Ukraine", "01.12.3456"));
            nb.globalID = nb.people.Count() + 1;


            while (true)
            {
                Console.Clear();
                Notebook.Menu();
                string menuChoice = Console.ReadLine();
                Console.Clear();
                while (menuChoice == "c")
                {
                    Console.Clear();
                    Person p = nb.AddPerson(nb.globalID, null);
                    nb.people.Add(p);
                    nb.globalID++;
                    Console.WriteLine("\n A person was added");
                    Console.WriteLine("\n -Create again \t\t<c>");
                    Notebook.Back();
                    menuChoice = Console.ReadLine();
                }
                while (menuChoice == "e")
                {
                    menuChoice = nb.EditPerson();
                    Console.Clear();
                }
                while (menuChoice == "d")
                {
                    menuChoice = nb.DeletePerson();
                    Console.Clear();
                }
                while (menuChoice == "i")
                {
                    menuChoice = nb.ViewPerson();
                    Console.Clear();
                }
                while (menuChoice == "a")
                {
                    menuChoice = nb.ViewPeople();
                    Console.Clear();
                }
                if (menuChoice == "x")
                    break;
            }
        }
    }
}