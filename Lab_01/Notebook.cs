﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_01
{
    public class Notebook
    {
        public List<Person> people = new List<Person>();
        public int globalID = 0;

        public Person AddPerson(int id, Person prevPerson)
        {
            Person p = new Person();
            string temp = "";
            p.ID = id;
            if (prevPerson != null)
                Console.WriteLine(" ---EDITING A PERSON---" +
                "\n fields marked with * are necessary\n");
            else Console.WriteLine(" ---CREATING A PERSON---" +
                "\n fields marked with * are necessary\n");

            while(temp == "")
            {
                Console.Write(" * Name: ");
                if ( prevPerson != null)
                    Console.Write("{0} > ", prevPerson.Name);
                temp = Console.ReadLine();
                if (temp == "")
                    Console.WriteLine(" Please enter name!");
            }
            p.Name = temp;
            temp = "";

            while (temp == "")
            {
                Console.Write(" * Last name: ");
                if (prevPerson != null)
                    Console.Write("{0} > ", prevPerson.LastName);
                temp = Console.ReadLine();
                if (temp == "")
                    Console.WriteLine(" Please enter last name!");
            }
            p.LastName = temp;
            temp = "";

            Console.Write("   Mid name: ");
            if (prevPerson != null)
                Console.Write("{0} > ", prevPerson.MidName);
            temp = Console.ReadLine();
            if (temp == "")
            {
                Console.WriteLine("\t---Mid name skipped---");
                p.MidName = "---";
            }
            else
                p.MidName = temp;

            while (temp == "" || !checkPhone(temp))
            {
                Console.Write(" * Phone (numbers only): ");
                if (prevPerson != null)
                    Console.Write("{0} > ", prevPerson.Phone);
                temp = Console.ReadLine();
                if (temp == "" || !checkPhone(temp))
                    Console.WriteLine(" Please enter phone number!");
            }
            p.Phone = int.Parse(temp);
            temp = "";

            while (temp == "")
            {
                Console.Write(" * Country: ");
                if (prevPerson != null)
                    Console.Write("{0} > ", prevPerson.Country);
                temp = Console.ReadLine();
                if (temp == "")
                    Console.WriteLine(" Please enter country!");
            }
            p.Country = temp;
            temp = "";

            bool ok = false;
            while (!ok)
            {
                Console.Write("   Birthday (DD.MM.YYYY): ");
                if (prevPerson != null)
                    Console.Write("{0} > ", prevPerson.BDay);
                temp = Console.ReadLine();
                if (BDayCheck(temp))
                {
                    p.BDay = temp;
                    ok = true;
                }
                else if (temp == "")
                {
                    Console.WriteLine("\t---Birthday skipped---");
                    p.BDay = "---";
                    ok = true;
                }
                else
                    Console.WriteLine(" Please correct or skip birthday!");
            }

            Console.Write("   Company: ");
            if (prevPerson != null)
                Console.Write("{0} > ", prevPerson.Company);
            temp = Console.ReadLine();
            if (temp == "")
            {
                Console.WriteLine("\t---Company skipped---");
                p.Company = "---";
            }
            else
                p.Company = temp;

            Console.Write("   Job: ");
            if (prevPerson != null)
                Console.Write("{0} > ", prevPerson.Job);
            temp = Console.ReadLine();
            if (temp == "")
            {
                Console.WriteLine("\t---Job skipped---");
                p.Job = "---";
            }
            else
                p.Job = temp;

            Console.Write("   Notes: ");
            if (prevPerson != null)
                Console.Write("{0} > ", prevPerson.Notes);
            temp = Console.ReadLine();
            if (temp == "")
            {
                Console.WriteLine("\t---Notes skipped---");
                p.Notes = "---";
            }
            else
                p.Notes = temp;

            if (prevPerson != null)
                people.Remove(prevPerson);
            return p;
        }
        public string EditPerson()
        {
            int chosenOne = ChoosePerson();
            if (chosenOne != 0)
            {
                Console.Clear();
                Person p = AddPerson(globalID, FindPerson(chosenOne));
                if (p != null)
                {
                    people.Add(p);
                    globalID++;
                    Console.WriteLine("\n A person was edited");
                }
                else
                    Console.WriteLine("\n Something went wrong!");
                Console.WriteLine("\n -Edit again \t\t<e>");
                Back();
                return Console.ReadLine();
            }
            else
            {
                Console.WriteLine("\n Wrong number!");
                Console.WriteLine("\n -Try again \t\t<e>");
                Back();
                return Console.ReadLine();
            }
        }
        public string DeletePerson(){
            int chosenOne = ChoosePerson();
            if (chosenOne != 0)
            {
                people.Remove(FindPerson(chosenOne));
                Console.WriteLine("\n A person was deleted");
                Console.WriteLine("\n -Delete again \t\t<d>");
            }
            else
            {
                Console.WriteLine("\n Wrong number!");
                Console.WriteLine("\n -Try again \t\t<d>");
            }
            Back();
            return Console.ReadLine();
        }
        public string ViewPerson() {
            int chosenOne = ChoosePerson();
            if (chosenOne != 0)
            {
                Person p = FindPerson(chosenOne);
                Console.Clear();
                Console.WriteLine(" ---PERSON INFO---\n");
                Console.WriteLine(p.FullInfo());
                Console.WriteLine("\n -Back to list \t\t<i>");
            }
            else
            {
                Console.WriteLine("\n Wrong number!");
                Console.WriteLine("\n -Try again \t\t<i>");
            }
            Back();
            return Console.ReadLine();
        }
        public string ViewPeople()
        {
            Console.WriteLine(" ---ALL PEOPLE---\n");
            ShortList();
            Console.WriteLine();
            Back();
            return Console.ReadLine();
        }

        public void ShortList()
        {
            int i = 1;
            foreach(Person p in people)
            {
                Console.WriteLine(" № {0}\n{1}", i, p.ShortInfo());
                Console.WriteLine(" -----------------------");
                i++;
            }
        }
        public int ChoosePerson()
        {
            Console.WriteLine(" ---CHOOSE A PERSON---\n");
            ShortList();
            Console.WriteLine("\n ---ENTER A NUMBER---");
            try
            {
                int i = int.Parse(Console.ReadLine());
                if (i > 0 && i <= people.Count && i.ToString() != "")
                    return this.people[i - 1].ID;
                else
                    return 0;
            }
            catch
            {
                return 0;
            }
        }
        public Person FindPerson(int id)
        {
            foreach (Person p in people)
            {
                if (p.ID == id)
                    return p;
            }
            return null;
        }

        private bool BDayCheck(string bDay)
        {
            try
            {
                DateTime.ParseExact(bDay, "dd.MM.yyyy", null);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool checkPhone(string phone)
        {
            try
            {
                int.Parse(phone);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void Menu()
        {
            Console.WriteLine(" ---MY NOTEBOOK---\n\n" +
                " -Add new person\t<c>\n" +
                " -Edit a person\t\t<e>\n" +
                " -Delete a person\t<d>\n" +
                " -View a person\t\t<i>\n" +
                " -View all people\t<a>\n\n" +
                " -Exit\t\t\t<x>\n\n" +
                " ---ENTER A SYMBOL---");
        }
        public static void Back()
        {
            Console.WriteLine(" -Back to menu\t\t<any symbol>\n\n" +
                " ---ENTER A SYMBOL---");
        }
    }
}