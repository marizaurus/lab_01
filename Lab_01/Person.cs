﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_01
{
    public class Person
    {
        #region fields

        public int ID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string MidName { get; set; }
        public int Phone { get; set; }
        public string Country { get; set; }
        public string BDay { get; set; }
        public string Company { get; set; }
        public string Job { get; set; }
        public string Notes { get; set; }

        #endregion

        public Person(int id, string name, string lastName, int phone, string country, string bDay)
        {
            this.ID = id;
            this.Name = name;
            this.LastName = lastName;
            this.MidName = "---";
            this.Phone = phone;
            this.Country = country;
            this.BDay = bDay;
            this.Company = "---";
            this.Job = "---";
            this.Notes = "---";
        }
        public Person(int id, string name, string lastName, string midName, int number, 
            string country, string bDay, string company, string job, string notes)
        {
            this.ID = id;
            this.Name = name;
            this.LastName = lastName;
            this.MidName = midName;
            this.Phone = number;
            this.Country = country;
            this.BDay = bDay;
            this.Company = company;
            this.Job = job;
            this.Notes = notes;
        }
        public Person() { }

        public string ShortInfo()
        {
            return string.Format(" Name: {0}\n Last name: {1}\n Phone: {2}", Name, LastName, Phone);
        }
        public string FullInfo()
        {
            return string.Format(" Name: {0}\n Last name: {1}\n Mid name: {2}\n Phone: {3}\n Country: {4}" +
                "\n BDay: {5}\n Company: {6}\n Job: {7}\n Notes: {8}",
                Name, LastName, MidName, Phone, Country, BDay, Company, Job, Notes);
        }
    }
}